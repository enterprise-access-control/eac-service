package ua.nure.pavlenko.eac.processor.exception;

public class EventProcessingException extends Exception {

    public EventProcessingException() {
    }

    public EventProcessingException(String message) {
        super(message);
    }
}
