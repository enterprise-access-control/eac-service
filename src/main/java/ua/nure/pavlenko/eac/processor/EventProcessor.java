package ua.nure.pavlenko.eac.processor;

import ua.nure.pavlenko.eac.api.EventProcessingResult;

public interface EventProcessor<EVENT_TYPE> {

    EventProcessingResult processEvent(EVENT_TYPE event_type);
}
