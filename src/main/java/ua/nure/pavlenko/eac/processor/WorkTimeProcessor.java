package ua.nure.pavlenko.eac.processor;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import ua.nure.pavlenko.eac.repository.entity.EnterpriseRoomEntity;
import ua.nure.pavlenko.eac.repository.entity.RFIDDeviceOperationEntity;
import ua.nure.pavlenko.eac.repository.entity.UserCommitmentEntity;
import ua.nure.pavlenko.eac.repository.entity.UserEntity;
import ua.nure.pavlenko.eac.service.MailService;
import ua.nure.pavlenko.eac.service.RFIDDeviceOperationService;
import ua.nure.pavlenko.eac.service.UserService;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Component
public class WorkTimeProcessor {

    private final UserService userService;
    private final RFIDDeviceOperationService operationService;
    private final MailService mailService;

    public WorkTimeProcessor(UserService userService,
            RFIDDeviceOperationService operationService,
            MailService mailService) {
        this.userService = userService;
        this.operationService = operationService;
        this.mailService = mailService;
    }

    @Scheduled(cron = "0 0 18 * * MON-FRI")
    public void calculateWorkTimeLeftForUsers() {
        List<UserEntity> users = userService.findAllNotBannedWithCommitments();

        users.forEach(this::calculateUserWorkTimeInCurrentMonth);

        mailService.sendWorkTimeEmailForAdmins(userService.findAllNotBannedAdmins(), users);
    }


    private void calculateUserWorkTimeInCurrentMonth(UserEntity user) {
        Long calculatedWorkTimeInCurrentMonth = 0L;

        Set<EnterpriseRoomEntity> trackedUserRooms = user.getUserCommitments().stream()
                                                         .map(UserCommitmentEntity::getEnterpriseRoom)
                                                         .collect(Collectors.toSet());
        List<RFIDDeviceOperationEntity> filteredOperations = operationService.findForUserInThisMonth(user)
                                                                             .stream()
                                                                             .sorted(RFIDDeviceOperationEntity::compareTo)
                                                                             .collect(Collectors.toList());
        for (int i = 0; i < filteredOperations.size() - 1; i++) {
            RFIDDeviceOperationEntity operation = filteredOperations.get(i);
            if (operation.getEnterRoom().isTrackingTimeZone()
                    && trackedUserRooms.contains(operation.getEnterRoom())) {
                calculatedWorkTimeInCurrentMonth
                        = increaseUserWorkTime(operation, filteredOperations.get(i + 1), calculatedWorkTimeInCurrentMonth);
            }
        }

        user.setWorkedThisMonth(calculatedWorkTimeInCurrentMonth);
        userService.save(user);

        mailService.sendWorkTimeEmail(user);
    }

    private Long increaseUserWorkTime(RFIDDeviceOperationEntity inPastOperation,
            RFIDDeviceOperationEntity newestOperation, Long calculatedTime) {
        return calculatedTime + (newestOperation.getOperationTimestamp().getTime() - inPastOperation.getOperationTimestamp().getTime());
    }
}
