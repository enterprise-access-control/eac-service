package ua.nure.pavlenko.eac.processor;

import org.springframework.stereotype.Component;
import ua.nure.pavlenko.eac.api.EventProcessingResult;
import ua.nure.pavlenko.eac.api.RFIDOperationEvent;
import ua.nure.pavlenko.eac.processor.exception.EventProcessingException;
import ua.nure.pavlenko.eac.repository.entity.CertificateEntity;
import ua.nure.pavlenko.eac.repository.entity.RFIDDeviceEntity;
import ua.nure.pavlenko.eac.repository.entity.RFIDDeviceOperationEntity;
import ua.nure.pavlenko.eac.repository.entity.UserEntity;
import ua.nure.pavlenko.eac.service.RFIDDeviceOperationService;
import ua.nure.pavlenko.eac.service.RFIDDeviceService;
import ua.nure.pavlenko.eac.service.UserService;
import ua.nure.pavlenko.eac.util.RfidDeviceUtils;
import ua.nure.pavlenko.eac.validator.access.CertificateAccessValidator;

import java.sql.Timestamp;
import java.util.Set;

@Component
public class RFIDDeviceOperationProcessor implements EventProcessor<RFIDOperationEvent> {

    private final UserService userService;
    private final RFIDDeviceService rfidDeviceService;
    private final CertificateAccessValidator certificateAccessValidator;
    private final RFIDDeviceOperationService rfidDeviceOperationService;

    public RFIDDeviceOperationProcessor(
            UserService userService,
            RFIDDeviceService rfidDeviceService,
            CertificateAccessValidator certificateAccessValidator,
            RFIDDeviceOperationService rfidDeviceOperationService) {
        this.userService = userService;
        this.rfidDeviceService = rfidDeviceService;
        this.certificateAccessValidator = certificateAccessValidator;
        this.rfidDeviceOperationService = rfidDeviceOperationService;
    }

    @Override
    public EventProcessingResult processEvent(RFIDOperationEvent event) {
        EventProcessingResult processingResult = new EventProcessingResult();
        try {
            RFIDDeviceOperationEntity operationEntity = buildRfidDeviceOperationEntity(event);
            rfidDeviceOperationService.createEvent(operationEntity);
            return processingResult.setSuccess(true);
        } catch (Exception exception) {
            processingResult.setSuccess(false)
                            .addExceptionMessage(exception.getMessage())
                            .addExceptionMessage("Event is invalid.");
            return processingResult;
        }
    }

    private RFIDDeviceOperationEntity buildRfidDeviceOperationEntity(RFIDOperationEvent event) throws Exception {
        UserEntity user = userService.findByUuid(event.getUserUuid(), true);
        RFIDDeviceEntity rfidDeviceEntity = rfidDeviceService.findByUuid(event.getRfidDeviceUuid(), true);
        Set<CertificateEntity> expectedCertificates =
                RfidDeviceUtils.getExpectedUserCertificatesByOperationType(rfidDeviceEntity, event.getOperationType());

        if (certificateAccessValidator.validateAccess(expectedCertificates, user)) {
            return new RFIDDeviceOperationEntity().setUserEntity(user)
                                                  .setRfidDeviceEntity(rfidDeviceEntity)
                                                  .setOperationType(event.getOperationType())
                                                  .setOperationTimestamp(new Timestamp(event.getTimestamp()));

        }
        throw new EventProcessingException();
    }
}
