package ua.nure.pavlenko.eac.util;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.http.ResponseEntity;
import ua.nure.pavlenko.eac.api.EventProcessingResult;

import java.util.Collection;
import java.util.List;

public class ControllerUtils {

    public static ResponseEntity<EventProcessingResult> buildEventProcessingResponse(
            EventProcessingResult processingResult) {
        if (processingResult != null && processingResult.isSuccess()) {
            return ResponseEntity.ok(processingResult);
        }
        return ResponseEntity.badRequest()
                             .body(processingResult);
    }

    public static <T> ResponseEntity<T> buildResponseEntity(T modelObject) {
        if (modelObject == null) {
            return ResponseEntity.notFound()
                                 .build();
        }
        return ResponseEntity.ok(modelObject);
    }

    public static <T> ResponseEntity<Collection<T>> buildResponseEntity(List<T> modelObject) {
        if (CollectionUtils.isEmpty(modelObject)) {
            return ResponseEntity.notFound()
                                 .build();
        }
        return ResponseEntity.ok(modelObject);
    }
}
