package ua.nure.pavlenko.eac.util;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import ua.nure.pavlenko.eac.repository.entity.CertificateEntity;
import ua.nure.pavlenko.eac.repository.entity.RFIDDeviceEntity;
import ua.nure.pavlenko.eac.repository.entity.RFIDDeviceOperationEntity;
import ua.nure.pavlenko.eac.repository.entity.RFIDDeviceOperationType;

import java.util.Collections;
import java.util.Set;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class RfidDeviceUtils {

    public static Set<CertificateEntity> getExpectedUserCertificatesByOperationType(
            RFIDDeviceEntity device,
            RFIDDeviceOperationType operationType) {
        switch (operationType) {
            case ENTER_APPROVED:
                return device.getEnterRoom().getCertificates();
            case EXIT_APPROVED:
                return device.getExitRoom().getCertificates();
            default:
                return Collections.emptySet();
        }
    }

    public static String getRoomCodeByOperation(RFIDDeviceOperationEntity operation) {
        switch (operation.getOperationType()) {
            case ENTER_APPROVED:
                return operation.getRfidDeviceEntity().getEnterRoom().getCode();
            case EXIT_APPROVED:
                return operation.getRfidDeviceEntity().getExitRoom().getCode();
            default:
                return "N/A";
        }
    }
}
