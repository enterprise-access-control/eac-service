package ua.nure.pavlenko.eac.util;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import ua.nure.pavlenko.eac.repository.entity.BaseEntity;

import javax.persistence.EntityNotFoundException;
import java.util.Optional;
import java.util.function.Function;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class ServiceUtils {

    public static <E extends BaseEntity, SP> E findByParameter(
            Function<SP, Optional<E>> finder,
            SP searchParameter,
            Boolean throwIfNotFound) {
        Optional<E> optionalEntity = finder.apply(searchParameter);
        if (throwIfNotFound) {
            return optionalEntity.orElseThrow(EntityNotFoundException::new);
        }
        return optionalEntity.orElse(null);
    }
}