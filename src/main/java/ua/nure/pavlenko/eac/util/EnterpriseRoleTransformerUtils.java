package ua.nure.pavlenko.eac.util;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import ua.nure.pavlenko.eac.api.EnterpriseRole;
import ua.nure.pavlenko.eac.repository.entity.EnterpriseRoleEntity;

import static ua.nure.pavlenko.eac.util.CertificateTransformerUtils.transformToDefaultCertificates;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class EnterpriseRoleTransformerUtils {

    public static EnterpriseRole transformToEnterpriseRole(EnterpriseRoleEntity roleEntity) {
        return new EnterpriseRole().setCode(roleEntity.getCode())
                                   .setDescription(roleEntity.getDescription())
                                   .setCertificates(transformToDefaultCertificates(roleEntity.getCertificates()));
    }
}
