package ua.nure.pavlenko.eac.util;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import ua.nure.pavlenko.eac.api.Certificate;
import ua.nure.pavlenko.eac.repository.entity.CertificateEntity;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class CertificateTransformerUtils {

    public static Certificate transformDefaultCertificateByEntity(CertificateEntity certificateEntity) {
        return new Certificate().setCode(certificateEntity.getCode())
                                .setDescription(certificateEntity.getDescription());
    }

    public static List<Certificate> transformToDefaultCertificates(Collection<CertificateEntity> certificates) {
        return certificates.stream()
                           .map(CertificateTransformerUtils::transformDefaultCertificateByEntity)
                           .collect(Collectors.toList());
    }
}
