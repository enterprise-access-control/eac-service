package ua.nure.pavlenko.eac.util;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import ua.nure.pavlenko.eac.api.UserRfidOperation;
import ua.nure.pavlenko.eac.repository.entity.RFIDDeviceOperationEntity;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class RFIDOperationTransformerUtils {

    public static UserRfidOperation transformToUserRfidOperation(RFIDDeviceOperationEntity operationEntity) {
        return new UserRfidOperation().setDeviceCode(operationEntity.getRfidDeviceEntity().getCode())
                                      .setCreationTimestamp(operationEntity.getCreationTimestamp())
                                      .setOperationTimestamp(operationEntity.getOperationTimestamp())
                                      .setLogin(operationEntity.getUserEntity().getLogin())
                                      .setOperationType(operationEntity.getOperationType())
                                      .setCurrentRoomCode(RfidDeviceUtils.getRoomCodeByOperation(operationEntity));
    }
}
