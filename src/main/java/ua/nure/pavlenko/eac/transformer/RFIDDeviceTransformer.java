package ua.nure.pavlenko.eac.transformer;

import org.springframework.stereotype.Component;
import ua.nure.pavlenko.eac.api.RFIDDevice;
import ua.nure.pavlenko.eac.api.RFIDDeviceDetailed;
import ua.nure.pavlenko.eac.repository.entity.RFIDDeviceEntity;
import ua.nure.pavlenko.eac.service.RFIDDeviceOperationService;
import ua.nure.pavlenko.eac.service.RFIDDeviceService;
import ua.nure.pavlenko.eac.util.RFIDOperationTransformerUtils;

import java.util.List;
import java.util.stream.Collectors;

import static ua.nure.pavlenko.eac.util.CertificateTransformerUtils.transformToDefaultCertificates;

@Component
public class RFIDDeviceTransformer {

    private final RFIDDeviceService deviceService;
    private final RFIDDeviceOperationService operationService;

    public RFIDDeviceTransformer(RFIDDeviceService deviceService, RFIDDeviceOperationService operationService) {
        this.deviceService = deviceService;
        this.operationService = operationService;
    }

    public List<RFIDDevice> findAll() {
        return deviceService.findAll().stream()
                            .map(this::transformToRfidDevice)
                            .collect(Collectors.toList());
    }

    public RFIDDeviceDetailed findDetailedByCode(String code) {
        return transformToRfidDeviceDetailed(deviceService.findByCode(code, true));
    }

    private RFIDDevice transformToRfidDevice(RFIDDeviceEntity deviceEntity) {
        return new RFIDDeviceDetailed().setCode(deviceEntity.getCode())
                                       .setEnterRoomCode(deviceEntity.getEnterRoom().getCode())
                                       .setExitRoomCode(deviceEntity.getExitRoom().getCode());
    }

    private RFIDDeviceDetailed transformToRfidDeviceDetailed(RFIDDeviceEntity deviceEntity) {
        return ((RFIDDeviceDetailed) transformToRfidDevice(deviceEntity))
                .setUuid(deviceEntity.getUuid())
                .setCreationTimestamp(deviceEntity.getCreationTimestamp())
                .setUpdatedTimestamp(deviceEntity.getUpdateTimestamp())
                .setExpectedEnterCertificates(transformToDefaultCertificates(deviceEntity.getEnterRoom()
                                                                                         .getCertificates()))
                .setExpectedExitCertificates(transformToDefaultCertificates(deviceEntity.getExitRoom()
                                                                                        .getCertificates()))
                .setLastUserOperations(operationService.findLastForDevice(deviceEntity).stream()
                                                       .map(RFIDOperationTransformerUtils::transformToUserRfidOperation)
                                                       .collect(Collectors.toList()));
    }
}
