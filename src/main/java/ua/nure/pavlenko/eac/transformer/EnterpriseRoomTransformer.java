package ua.nure.pavlenko.eac.transformer;

import org.springframework.stereotype.Component;
import ua.nure.pavlenko.eac.api.EnterpriseRoom;
import ua.nure.pavlenko.eac.repository.entity.EnterpriseRoomEntity;
import ua.nure.pavlenko.eac.service.EnterpriseRoomService;
import ua.nure.pavlenko.eac.util.CertificateTransformerUtils;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class EnterpriseRoomTransformer {

    private final EnterpriseRoomService enterpriseRoomService;

    public EnterpriseRoomTransformer(EnterpriseRoomService enterpriseRoomService) {
        this.enterpriseRoomService = enterpriseRoomService;
    }

    public EnterpriseRoom findByCode(String code) {
        return toEnterpriseRoom(enterpriseRoomService.findByCode(code, true));
    }

    public List<EnterpriseRoom> findAll() {
        return enterpriseRoomService.findAll()
                                    .stream()
                                    .map(this::toEnterpriseRoom)
                                    .collect(Collectors.toList());
    }

    public EnterpriseRoom toEnterpriseRoom(EnterpriseRoomEntity roomEntity) {
        return new EnterpriseRoom().setCode(roomEntity.getCode())
                                   .setTrackingTimeZone(roomEntity.isTrackingTimeZone())
                                   .setCertificates(roomEntity.getCertificates().stream()
                                                              .map(CertificateTransformerUtils::transformDefaultCertificateByEntity)
                                                              .collect(Collectors.toList()));
    }
}
