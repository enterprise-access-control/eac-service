package ua.nure.pavlenko.eac.transformer;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.stereotype.Component;
import ua.nure.pavlenko.eac.api.User;
import ua.nure.pavlenko.eac.api.UserDetailed;
import ua.nure.pavlenko.eac.repository.entity.UserEntity;
import ua.nure.pavlenko.eac.service.RFIDDeviceOperationService;
import ua.nure.pavlenko.eac.service.UserService;

import java.util.List;
import java.util.stream.Collectors;

import static ua.nure.pavlenko.eac.util.CertificateTransformerUtils.transformToDefaultCertificates;
import static ua.nure.pavlenko.eac.util.EnterpriseRoleTransformerUtils.transformToEnterpriseRole;
import static ua.nure.pavlenko.eac.util.RFIDOperationTransformerUtils.transformToUserRfidOperation;
import static ua.nure.pavlenko.eac.util.TimeUtils.convertMillisToHours;

@Component
public class UserTransformer {

    private final UserService userService;
    private final RFIDDeviceOperationService operationService;

    public UserTransformer(UserService userService, RFIDDeviceOperationService operationService) {
        this.userService = userService;
        this.operationService = operationService;
    }

    public List<User> findAll() {
        return userService.findAll().stream()
                          .map(this::transformToUser)
                          .collect(Collectors.toList());
    }

    public UserDetailed findByLoginDetailed(String login) {
        return transformToUserDetailed(userService.findByLogin(login, true));
    }

    private User transformToUser(UserEntity userEntity) {
        return new UserDetailed().setEmail(userEntity.getEmail())
                                 .setFirstName(userEntity.getName())
                                 .setSecondName(userEntity.getSurname())
                                 .setLogin(userEntity.getLogin())
                                 .setBanned(userEntity.isBanned())
                                 .setRole(userEntity.getSystemRole().getRole())
                                 .setRegistrationTimestamp(userEntity.getCreationTimestamp())
                                 .setLastUpdateTimestamp(userEntity.getUpdateTimestamp());
    }

    private UserDetailed transformToUserDetailed(UserEntity userEntity) {
        return ((UserDetailed) transformToUser(userEntity))
                .setUuid(userEntity.getUuid())
                .setCertificates(CollectionUtils.isNotEmpty(userEntity.getCertificates())
                                         ? transformToDefaultCertificates(userEntity.getCertificates())
                                         : null)
                .setEnterpriseRole(userEntity.getEnterpriseRole() != null
                                           ? transformToEnterpriseRole(userEntity.getEnterpriseRole())
                                           : null)
                .setUserRfidOperation(transformToUserRfidOperation(operationService.findUserLastOperation(userEntity)))
                .setWorkedHoursThisMonth(convertMillisToHours(userEntity.getWorkedThisMonth()))
                .setEstimatedHoursThisMonth(convertMillisToHours(userEntity.getUserEstimatedWorkHoursInMillis()));

    }
}
