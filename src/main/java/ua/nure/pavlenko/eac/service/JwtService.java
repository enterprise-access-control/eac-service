package ua.nure.pavlenko.eac.service;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import ua.nure.pavlenko.eac.repository.entity.UserEntity;

import java.util.Calendar;
import java.util.Date;

@Service
public class JwtService {

    private final String key;
    private final int sessionTime;

    public JwtService(@Autowired @Value("${spring.security.token.secret-key}") String key,
            @Autowired @Value("${spring.security.token.session-time}") int sessionTime) {
        this.key = key;
        this.sessionTime = sessionTime;
    }

    public String createToken(UserEntity user) {
        final Calendar expirationCalendar = Calendar.getInstance();
        expirationCalendar.add(Calendar.MILLISECOND, sessionTime);
        final Date expirationDate = Date.from(expirationCalendar.toInstant());
        return Jwts.builder()
                   .setSubject(user.getLogin())
                   .setExpiration(expirationDate)
                   .signWith(SignatureAlgorithm.HS512, key)
                   .compact();
    }

    public boolean validateToken(String token) {
        try {
            Jwts.parser().setSigningKey(key).parseClaimsJws(token);
            return true;
        } catch (Exception exception) {
            //  In case token is invalid
            return false;
        }
    }

    public String obtainSubject(String token) {
        final Claims claims = Jwts.parser().setSigningKey(key).parseClaimsJws(token).getBody();
        return claims.getSubject();
    }
}
