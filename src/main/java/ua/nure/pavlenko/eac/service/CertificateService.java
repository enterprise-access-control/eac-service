package ua.nure.pavlenko.eac.service;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ua.nure.pavlenko.eac.api.Certificate;
import ua.nure.pavlenko.eac.repository.CertificateRepository;
import ua.nure.pavlenko.eac.repository.entity.CertificateEntity;
import ua.nure.pavlenko.eac.util.CertificateTransformerUtils;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class CertificateService {

    private final CertificateRepository certificateRepository;

    public CertificateService(CertificateRepository certificateRepository) {
        this.certificateRepository = certificateRepository;
    }

    @Transactional(readOnly = true)
    public List<Certificate> findAllModels() {
        return certificateRepository.findAll()
                                    .stream()
                                    .map(CertificateTransformerUtils::transformDefaultCertificateByEntity)
                                    .collect(Collectors.toList());
    }

    @Transactional(readOnly = true)
    public Certificate findByCode(String code) {
        return certificateRepository.findByCodeIgnoreCase(code)
                                    .map(CertificateTransformerUtils::transformDefaultCertificateByEntity)
                                    .orElse(null);
    }

    @Transactional(readOnly = true)
    public Set<CertificateEntity> findAll(List<Certificate> certificates) {
        return certificateRepository.findAllByCodeInIgnoreCase(certificates.stream()
                                                                           .map(Certificate::getCode)
                                                                           .collect(Collectors.toList()));
    }

    @Transactional
    public void createCertificate(Certificate certificate) {
        if (certificateRepository.existsByCode(certificate.getCode())) {
            throw new IllegalArgumentException("Certificate with such code already present");
        }
        CertificateEntity certificateEntity = new CertificateEntity().setCode(certificate.getCode())
                                                                     .setDescription(certificate.getDescription());
        certificateRepository.save(certificateEntity);
    }
}
