package ua.nure.pavlenko.eac.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;
import ua.nure.pavlenko.eac.repository.entity.UserEntity;

import java.util.List;

import static ua.nure.pavlenko.eac.util.TimeUtils.convertMillisToHours;

@Service
@Slf4j
public class MailService {

    private final JavaMailSender mailSender;

    public MailService(JavaMailSender mailSender) {
        this.mailSender = mailSender;
    }

    public void sendWorkTimeEmail(UserEntity user) {
        try {
            SimpleMailMessage mailMessage = new SimpleMailMessage();
            mailMessage.setFrom("eac.service.work@gmail.com");
            mailMessage.setTo(user.getEmail());
            mailMessage.setSubject("Tracked work time this mount");
            mailMessage.setText(String.format("You worked %s/%s hours this month",
                                              convertMillisToHours(user.getWorkedThisMonth()),
                                              convertMillisToHours(user.getUserEstimatedWorkHoursInMillis())));
            mailSender.send(mailMessage);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    public void sendWorkTimeEmailForAdmins(List<UserEntity> admins, List<UserEntity> users) {
        try {
            SimpleMailMessage mailMessage = new SimpleMailMessage();
            mailMessage.setFrom("eac.service.work@gmail.com");
            mailMessage.setTo(admins.stream()
                                    .map(UserEntity::getEmail)
                                    .toArray(String[]::new));
            mailMessage.setSubject("Tracked work time this mount");
            mailMessage.setText(generateWorkTimesheet(users));
            mailSender.send(mailMessage);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    private String generateWorkTimesheet(List<UserEntity> users) {
        StringBuilder stringBuilder = new StringBuilder();
        for (UserEntity user : users) {
            stringBuilder.append(String.format("User [email: %s, login: %s] worked %s/%s hours this month",
                                               user.getEmail(), user.getLogin(),
                                               convertMillisToHours(user.getWorkedThisMonth()),
                                               convertMillisToHours(user.getUserEstimatedWorkHoursInMillis())));
            stringBuilder.append(System.lineSeparator());
        }
        return stringBuilder.toString();
    }
}
