package ua.nure.pavlenko.eac.service;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ua.nure.pavlenko.eac.api.Certificate;
import ua.nure.pavlenko.eac.api.Token;
import ua.nure.pavlenko.eac.api.UserCreation;
import ua.nure.pavlenko.eac.api.UserSignIn;
import ua.nure.pavlenko.eac.repository.UserRepository;
import ua.nure.pavlenko.eac.repository.entity.SystemRole;
import ua.nure.pavlenko.eac.repository.entity.UserEntity;
import ua.nure.pavlenko.eac.security.exception.AuthException;
import ua.nure.pavlenko.eac.util.ServiceUtils;
import ua.nure.pavlenko.eac.validator.operation.BanOperationValidator;

import javax.persistence.EntityExistsException;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@Transactional
public class UserService {

    private final UserRepository userRepository;
    private final PasswordService passwordService;
    private final JwtService jwtService;
    private final SystemRoleService systemRoleService;
    private final BanOperationValidator banOperationValidator;
    private final CertificateService certificateService;
    private final EnterpriseRoleService enterpriseRoleService;

    public UserService(UserRepository userRepository,
            PasswordService passwordService,
            JwtService jwtService,
            SystemRoleService systemRoleService,
            BanOperationValidator banOperationValidator,
            CertificateService certificateService,
            EnterpriseRoleService enterpriseRoleService) {
        this.userRepository = userRepository;
        this.passwordService = passwordService;
        this.jwtService = jwtService;
        this.systemRoleService = systemRoleService;
        this.banOperationValidator = banOperationValidator;
        this.certificateService = certificateService;
        this.enterpriseRoleService = enterpriseRoleService;
    }

    public List<UserEntity> findAll() {
        return userRepository.findAll();
    }

    public List<UserEntity> findAllNotBannedWithCommitments() {
        return userRepository.findAllByBanned(false)
                             .stream()
                             .filter(user -> CollectionUtils.isNotEmpty(user.getUserCommitments()))
                             .collect(Collectors.toList());
    }

    public List<UserEntity> findAllNotBannedAdmins() {
        return userRepository.findAllByBannedAndSystemRole_Role(false, SystemRole.ADMIN);
    }

    public UserEntity findByUuid(UUID uuid, boolean throwIfNotFound) {
        return ServiceUtils.findByParameter(userRepository::findByUuid, uuid, throwIfNotFound);
    }

    public UserEntity findByEmail(String email, boolean throwIfNotFound) {
        return ServiceUtils.findByParameter(userRepository::findByEmail, email, throwIfNotFound);
    }

    public UserEntity findByLogin(String login, boolean throwIfNotFound) {
        return ServiceUtils.findByParameter(userRepository::findByLogin, login, throwIfNotFound);
    }

    public Optional<Token> signInUser(UserSignIn userSignIn) {
        final UserEntity userEntity = Optional.ofNullable(findByEmail(userSignIn.getEmail(), false))
                                              .orElseThrow(AuthException::new);
        if (!passwordService.verify(userSignIn.getPassword(), userEntity.getPassword())) {
            throw new AuthException();
        }
        return buildToken(userRepository.save(userEntity));
    }

    public void banUserByLogin(String login) {
        banUser(findByLogin(login, true));
    }

    public void banUserByUuid(UUID uuid) {
        banUser(findByUuid(uuid, true));
    }

    public void unbanUserByLogin(String login) {
        unbanUser(findByLogin(login, true));
    }

    public void unbanUserByUuid(UUID uuid) {
        unbanUser(findByUuid(uuid, true));
    }

    public void signupUser(UserCreation userCreation) {
        final Optional<UserEntity> entityOptional = userRepository.findByEmail(userCreation.getEmail());
        if (entityOptional.isPresent()) {
            throw new EntityExistsException("Such user already exists");
        }
        UserEntity userEntity = new UserEntity()
                .setEmail(userCreation.getEmail())
                .setLogin(userCreation.getLogin())
                .setName(userCreation.getFirstName())
                .setSurname(userCreation.getLastName())
                .setPassword(passwordService.encodePassword(userCreation.getPassword()))
                .setSystemRole(systemRoleService.findBySystemRole(userCreation.getRole(), true))
                .setBanned(false);
        userRepository.save(userEntity);
    }

    public UserEntity save(UserEntity userEntity) {
        return userRepository.save(userEntity);
    }

    public void updateUserCertificates(List<Certificate> certificates, UUID uuid) {
        UserEntity userEntity = findByUuid(uuid, true);
        userEntity.setCertificates(certificateService.findAll(certificates));
        userRepository.save(userEntity);
    }

    public void setUserEnterpriseRole(String enterpriseRoleCode, UUID uuid) {
        UserEntity userEntity = findByUuid(uuid, true);
        userEntity.setEnterpriseRole(enterpriseRoleService.findByCode(enterpriseRoleCode, true));
        userRepository.save(userEntity);
    }

    private Optional<Token> buildToken(UserEntity userEntity) {
        return Optional.of(new Token(jwtService.createToken(userEntity)));
    }

    private void banUser(UserEntity user) {
        if (user != null && !user.isBanned() && banOperationValidator.validateOperationAccess(user)) {
            user.setBanned(true);
            userRepository.save(user);
            return;
        }
        throw new IllegalArgumentException("User cannot be banned.");
    }

    private void unbanUser(UserEntity user) {
        if (user != null && user.isBanned() && banOperationValidator.validateOperationAccess(user)) {
            user.setBanned(false);
            userRepository.save(user);
            return;
        }
        throw new IllegalArgumentException("User cannot be unbanned.");
    }
}
