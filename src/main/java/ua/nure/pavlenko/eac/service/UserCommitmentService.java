package ua.nure.pavlenko.eac.service;

import org.springframework.stereotype.Service;
import ua.nure.pavlenko.eac.api.UserCommitment;
import ua.nure.pavlenko.eac.repository.UserCommitmentRepository;
import ua.nure.pavlenko.eac.repository.entity.EnterpriseRoomEntity;
import ua.nure.pavlenko.eac.repository.entity.UserCommitmentEntity;
import ua.nure.pavlenko.eac.repository.entity.UserEntity;

@Service
public class UserCommitmentService {

    private final UserCommitmentRepository userCommitmentRepository;
    private final UserService userService;
    private final EnterpriseRoomService roomService;

    public UserCommitmentService(UserCommitmentRepository userCommitmentRepository,
            UserService userService,
            EnterpriseRoomService roomService) {
        this.userCommitmentRepository = userCommitmentRepository;
        this.userService = userService;
        this.roomService = roomService;
    }

    public void createUserCommitment(UserCommitment userCommitment) {
        UserEntity userEntity = userService.findByLogin(userCommitment.getLogin(), true);
        EnterpriseRoomEntity roomEntity = roomService.findByCode(userCommitment.getRoomCode(), true);

        if (!roomEntity.isTrackingTimeZone()) {
            throw new IllegalArgumentException("User can`t have commitments in the untracked room");
        }

        UserCommitmentEntity commitmentEntity
                = new UserCommitmentEntity().setRequiredTimeInMonth(userCommitment.getRequiredTimeInMillisOnMonth())
                                            .setEnterpriseRoom(roomEntity)
                                            .setUser(userEntity);
        userCommitmentRepository.save(commitmentEntity);
    }
}
