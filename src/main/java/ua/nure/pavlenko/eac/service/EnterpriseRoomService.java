package ua.nure.pavlenko.eac.service;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ua.nure.pavlenko.eac.api.EnterpriseRoom;
import ua.nure.pavlenko.eac.repository.EnterpriseRoomRepository;
import ua.nure.pavlenko.eac.repository.entity.EnterpriseRoomEntity;
import ua.nure.pavlenko.eac.util.ServiceUtils;

import java.util.List;


@Service
public class EnterpriseRoomService {

    private final EnterpriseRoomRepository roomRepository;
    private final CertificateService certificateService;

    public EnterpriseRoomService(EnterpriseRoomRepository roomRepository, CertificateService certificateService) {
        this.roomRepository = roomRepository;
        this.certificateService = certificateService;
    }

    @Transactional
    public void createRoom(EnterpriseRoom enterpriseRoom) {
        if (roomRepository.existsByCode(enterpriseRoom.getCode())) {
            throw new IllegalArgumentException("Room with such code is present");
        }
        EnterpriseRoomEntity roomEntity = buildEnterpriseRoomEntity(enterpriseRoom);
        roomRepository.save(roomEntity);
    }

    @Transactional
    public void updateRoom(EnterpriseRoom enterpriseRoom) {
        EnterpriseRoomEntity roomEntity = findByCode(enterpriseRoom.getCode(), true);
        roomEntity.setTrackingTimeZone(enterpriseRoom.getTrackingTimeZone());
        if (CollectionUtils.isNotEmpty(enterpriseRoom.getCertificates())) {
            roomEntity.setCertificates(certificateService.findAll(enterpriseRoom.getCertificates()));
        }
        roomRepository.save(roomEntity);
    }

    @Transactional(readOnly = true)
    public EnterpriseRoomEntity findByCode(String code, boolean throwIfNotFound) {
        return ServiceUtils.findByParameter(roomRepository::findByCodeIgnoreCase, code, throwIfNotFound);
    }

    @Transactional(readOnly = true)
    public List<EnterpriseRoomEntity> findAll() {
        return roomRepository.findAll();
    }

    private EnterpriseRoomEntity buildEnterpriseRoomEntity(EnterpriseRoom enterpriseRoom) {
        EnterpriseRoomEntity roomEntity = new EnterpriseRoomEntity();
        roomEntity.setCode(enterpriseRoom.getCode());
        roomEntity.setTrackingTimeZone(enterpriseRoom.getTrackingTimeZone());
        if (CollectionUtils.isNotEmpty(enterpriseRoom.getCertificates())) {
            roomEntity.setCertificates(certificateService.findAll(enterpriseRoom.getCertificates()));
        }
        return roomEntity;
    }
}
