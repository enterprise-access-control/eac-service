package ua.nure.pavlenko.eac.service;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ua.nure.pavlenko.eac.api.RFIDDevice;
import ua.nure.pavlenko.eac.repository.RFIDDeviceRepository;
import ua.nure.pavlenko.eac.repository.entity.EnterpriseRoomEntity;
import ua.nure.pavlenko.eac.repository.entity.RFIDDeviceEntity;
import ua.nure.pavlenko.eac.util.ServiceUtils;

import java.util.List;
import java.util.UUID;

@Service
public class RFIDDeviceService {

    private final RFIDDeviceRepository rfidDeviceRepository;
    private final EnterpriseRoomService enterpriseRoomService;

    public RFIDDeviceService(RFIDDeviceRepository rfidDeviceRepository,
            EnterpriseRoomService enterpriseRoomService) {
        this.rfidDeviceRepository = rfidDeviceRepository;
        this.enterpriseRoomService = enterpriseRoomService;
    }

    @Transactional(readOnly = true)
    public List<RFIDDeviceEntity> findAll() {
        return rfidDeviceRepository.findAll();
    }

    @Transactional(readOnly = true)
    public RFIDDeviceEntity findByCode(String code, boolean throwIfNotFound) {
        return ServiceUtils.findByParameter(rfidDeviceRepository::findByCode, code, throwIfNotFound);
    }

    @Transactional(readOnly = true)
    public RFIDDeviceEntity findByUuid(UUID uuid, boolean throwIfNotFound) {
        return ServiceUtils.findByParameter(rfidDeviceRepository::findByUuid, uuid, throwIfNotFound);
    }

    @Transactional
    public void createRfidDevice(RFIDDevice rfidDevice) {
        if (!rfidDeviceRepository.existsByCode(rfidDevice.getCode())) {
            EnterpriseRoomEntity enterRoom = enterpriseRoomService.findByCode(rfidDevice.getEnterRoomCode(), true);
            EnterpriseRoomEntity exitRoom = enterpriseRoomService.findByCode(rfidDevice.getExitRoomCode(), true);
            RFIDDeviceEntity rfidDeviceEntity = new RFIDDeviceEntity().setCode(rfidDevice.getCode())
                                                                      .setEnterRoom(enterRoom)
                                                                      .setExitRoom(exitRoom);
            rfidDeviceRepository.save(rfidDeviceEntity);
            return;
        }
        throw new IllegalArgumentException("Rfid device with such code exists");
    }
}
