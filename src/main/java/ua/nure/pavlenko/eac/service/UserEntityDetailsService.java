package ua.nure.pavlenko.eac.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import ua.nure.pavlenko.eac.repository.entity.UserEntity;
import ua.nure.pavlenko.eac.security.model.UserEntityDetails;

import static java.util.Objects.isNull;

@Service
public class UserEntityDetailsService implements UserDetailsService {

    private final UserService userService;

    public UserEntityDetailsService(@Autowired UserService userService) {
        this.userService = userService;
    }

    @Override
    public UserEntityDetails loadUserByUsername(String login) {
        final UserEntity userEntity = userService.findByLogin(login, false);
        if (isNull(userEntity)) {
            throw new UsernameNotFoundException("Wrong login or password");
        }
        return new UserEntityDetails(userEntity);
    }
}