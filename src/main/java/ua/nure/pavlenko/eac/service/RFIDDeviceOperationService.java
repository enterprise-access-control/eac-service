package ua.nure.pavlenko.eac.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ua.nure.pavlenko.eac.repository.RFIDDeviceOperationRepository;
import ua.nure.pavlenko.eac.repository.entity.RFIDDeviceEntity;
import ua.nure.pavlenko.eac.repository.entity.RFIDDeviceOperationEntity;
import ua.nure.pavlenko.eac.repository.entity.UserEntity;
import ua.nure.pavlenko.eac.util.TimeUtils;

import java.sql.Timestamp;
import java.util.List;

@Service
public class RFIDDeviceOperationService {

    private final RFIDDeviceOperationRepository operationRepository;

    @Value("${operation-service.last_operation_threshold:86400000}")
    private Long lastOperationThreshold;

    public RFIDDeviceOperationService(RFIDDeviceOperationRepository operationRepository) {
        this.operationRepository = operationRepository;
    }

    @Transactional
    public void createEvent(RFIDDeviceOperationEntity operationEntity) {
        operationRepository.save(operationEntity);
    }

    public RFIDDeviceOperationEntity findUserLastOperation(UserEntity userEntity) {
        return operationRepository.findFirstByUserEntityOrderByOperationTimestampDesc(userEntity).orElse(null);
    }

    public List<RFIDDeviceOperationEntity> findLastForDevice(RFIDDeviceEntity deviceEntity) {
        Timestamp timestamp = new Timestamp(System.currentTimeMillis() - lastOperationThreshold);
        return operationRepository.findByRfidDeviceEntityAndOperationTimestampGreaterThan(deviceEntity, timestamp);
    }

    public List<RFIDDeviceOperationEntity> findForUserInThisMonth(UserEntity userEntity) {
        return operationRepository.findAllByOperationTimestampGreaterThanAndUserEntity(
                new Timestamp(TimeUtils.getStartMouthTimeInMillis()), userEntity);
    }
}
