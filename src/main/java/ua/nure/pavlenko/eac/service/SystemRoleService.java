package ua.nure.pavlenko.eac.service;

import org.springframework.stereotype.Service;
import ua.nure.pavlenko.eac.repository.SystemRoleRepository;
import ua.nure.pavlenko.eac.repository.entity.SystemRole;
import ua.nure.pavlenko.eac.repository.entity.SystemRoleEntity;
import ua.nure.pavlenko.eac.util.ServiceUtils;

@Service
public class SystemRoleService {

    private final SystemRoleRepository systemRoleRepository;

    public SystemRoleService(SystemRoleRepository systemRoleRepository) {
        this.systemRoleRepository = systemRoleRepository;
    }

    public SystemRoleEntity findBySystemRole(SystemRole role, boolean throwIfNotFound) {
        return ServiceUtils.findByParameter(systemRoleRepository::findByRole, role, throwIfNotFound);
    }
}
