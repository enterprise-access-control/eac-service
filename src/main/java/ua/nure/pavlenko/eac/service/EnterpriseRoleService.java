package ua.nure.pavlenko.eac.service;

import org.springframework.stereotype.Service;
import ua.nure.pavlenko.eac.api.EnterpriseRole;
import ua.nure.pavlenko.eac.repository.EnterpriseRoleRepository;
import ua.nure.pavlenko.eac.repository.entity.EnterpriseRoleEntity;
import ua.nure.pavlenko.eac.util.EnterpriseRoleTransformerUtils;
import ua.nure.pavlenko.eac.util.ServiceUtils;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class EnterpriseRoleService {

    private final EnterpriseRoleRepository enterpriseRoleRepository;
    private final CertificateService certificateService;

    public EnterpriseRoleService(EnterpriseRoleRepository enterpriseRoleRepository,
            CertificateService certificateService) {
        this.enterpriseRoleRepository = enterpriseRoleRepository;
        this.certificateService = certificateService;
    }

    public EnterpriseRoleEntity findByCode(String code, boolean throwIfNotFound) {
        return ServiceUtils.findByParameter(enterpriseRoleRepository::findByCode, code, throwIfNotFound);
    }

    public List<EnterpriseRole> findAll() {
        return enterpriseRoleRepository.findAll().stream()
                                       .map(EnterpriseRoleTransformerUtils::transformToEnterpriseRole)
                                       .collect(Collectors.toList());
    }

    public void createEnterpriseRole(EnterpriseRole enterpriseRole) {
        if (!enterpriseRoleRepository.existsByCode(enterpriseRole.getCode())) {
            EnterpriseRoleEntity enterpriseRoleEntity =
                    new EnterpriseRoleEntity().setCode(enterpriseRole.getCode())
                                              .setDescription(enterpriseRole.getDescription())
                                              .setCertificates(certificateService.findAll(enterpriseRole.getCertificates()));

            enterpriseRoleRepository.save(enterpriseRoleEntity);
            return;
        }
        throw new IllegalArgumentException("Enterprise role with such code exists");
    }

    public void deleteEnterpriseRoleByCode(String code) {
        EnterpriseRoleEntity enterpriseRoleEntity = findByCode(code, true);
        enterpriseRoleRepository.delete(enterpriseRoleEntity);
    }
}
