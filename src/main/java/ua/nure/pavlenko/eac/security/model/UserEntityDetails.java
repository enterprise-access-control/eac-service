package ua.nure.pavlenko.eac.security.model;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import ua.nure.pavlenko.eac.repository.entity.UserEntity;

import java.util.Collection;
import java.util.Collections;
import java.util.UUID;

public class UserEntityDetails implements UserDetails {

    private final UserEntity userEntity;

    public UserEntityDetails(UserEntity userEntity) {
        this.userEntity = userEntity;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return Collections.singletonList(new SimpleGrantedAuthority(userEntity.getSystemRole()
                                                                              .getRole()
                                                                              .getRoleName()));
    }

    @Override
    public String getPassword() {
        return userEntity.getPassword();
    }

    @Override
    public String getUsername() {
        return userEntity.getLogin();
    }

    public UUID getUserUuid() {
        return userEntity.getUuid();
    }

    @Override
    public boolean isAccountNonExpired() {
        //  User accounts is not expires yet
        return !userEntity.isBanned();
    }

    @Override
    public boolean isAccountNonLocked() {
        //  User accounts is not locks yet
        return !userEntity.isBanned();
    }

    @Override
    public boolean isCredentialsNonExpired() {
        //  User account credentials is not expires yet
        return !userEntity.isBanned();
    }

    @Override
    public boolean isEnabled() {
        //  User accounts is not disables yet
        return !userEntity.isBanned();
    }
}
