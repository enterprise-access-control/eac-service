package ua.nure.pavlenko.eac.security.exception;

import lombok.Getter;
import ua.nure.pavlenko.eac.api.ApiException;
import ua.nure.pavlenko.eac.api.AuthExceptionInformation;

public class AuthException extends RuntimeException {

    @Getter
    private final ApiException information;

    public AuthException() {
        information = new AuthExceptionInformation();
    }

    @Override
    public String getMessage() {
        return "Wrong email or password";
    }
}