package ua.nure.pavlenko.eac.security.filter;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.GenericFilterBean;
import ua.nure.pavlenko.eac.security.model.UserEntityDetails;
import ua.nure.pavlenko.eac.service.JwtService;
import ua.nure.pavlenko.eac.service.UserEntityDetailsService;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collection;
import java.util.List;

import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.util.StringUtils.hasText;

@Component
public class JwtFilter extends GenericFilterBean {

    private static final String BEARER_START = "Bearer ";
    private static final int BEARER_SUBSTRING_INDEX = 7;
    /**
     * Paths that filter won't check for token
     */
    private static final Collection<String> EXCLUDED_PATHS = List.of(
            "/api/auth",
            "/api/error",
            "api/rfid-device-operations"
    );
    private final JwtService jwtService;
    private final UserEntityDetailsService userDetailsService;

    public JwtFilter(@Autowired JwtService jwtService,
            @Autowired UserEntityDetailsService userDetailsService) {
        this.jwtService = jwtService;
        this.userDetailsService = userDetailsService;
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
            throws IOException, ServletException {
        final String path = ((HttpServletRequest) servletRequest).getRequestURI();
        for (String excludedPath : EXCLUDED_PATHS) {
            if (path.startsWith(excludedPath)) {
                filterChain.doFilter(servletRequest, servletResponse);
                return;
            }
        }
        final String token = obtainToken((HttpServletRequest) servletRequest);
        if (StringUtils.isNotEmpty(token) && jwtService.validateToken(token)) {
            final String login = jwtService.obtainSubject(token);
            final UserEntityDetails userDetails = userDetailsService.loadUserByUsername(login);
            final UsernamePasswordAuthenticationToken auth = new UsernamePasswordAuthenticationToken(
                    userDetails, null, userDetails.getAuthorities()
            );
            SecurityContextHolder.getContext().setAuthentication(auth);
            servletRequest.setAttribute("user_uuid", userDetails.getUserUuid());
            filterChain.doFilter(servletRequest, servletResponse);
            return;
        }
        ((HttpServletResponse) servletResponse).setStatus(HttpServletResponse.SC_FORBIDDEN);
    }

    private String obtainToken(HttpServletRequest request) {
        final String bearer = request.getHeader(AUTHORIZATION);
        if (hasText(bearer) && bearer.startsWith(BEARER_START)) {
            return bearer.substring(BEARER_SUBSTRING_INDEX);
        }
        return "";
    }
}