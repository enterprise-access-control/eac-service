package ua.nure.pavlenko.eac.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ua.nure.pavlenko.eac.api.Certificate;
import ua.nure.pavlenko.eac.api.User;
import ua.nure.pavlenko.eac.api.UserCreation;
import ua.nure.pavlenko.eac.api.UserDetailed;
import ua.nure.pavlenko.eac.service.UserService;
import ua.nure.pavlenko.eac.transformer.UserTransformer;

import java.util.Collection;
import java.util.List;
import java.util.UUID;

import static ua.nure.pavlenko.eac.util.ControllerUtils.buildResponseEntity;

@RestController
@RequestMapping("/users")
@PreAuthorize("hasRole('ADMIN')")
public class UserController {

    private final UserService userService;
    private final UserTransformer userTransformer;

    public UserController(UserService userService, UserTransformer userTransformer) {
        this.userService = userService;
        this.userTransformer = userTransformer;
    }

    @GetMapping
    public ResponseEntity<Collection<User>> getAll() {
        return buildResponseEntity(userTransformer.findAll());
    }

    @GetMapping("/{login}/details")
    public ResponseEntity<UserDetailed> findUserDetailedByLogin(@PathVariable("login") String login) {
        return buildResponseEntity(userTransformer.findByLoginDetailed(login));
    }

    @PostMapping("/{login}/banByLogin")
    public void banUserByLogin(@PathVariable("login") String login) {
        userService.banUserByLogin(login);
    }

    @PostMapping("/{login}/unbanByLogin")
    public void unbanUserByLogin(@PathVariable("login") String login) {
        userService.unbanUserByLogin(login);
    }

    @PostMapping("/{uuid}/banByUuid")
    public void banUserByUuid(@PathVariable("uuid") UUID uuid) {
        userService.banUserByUuid(uuid);
    }

    @PostMapping("/{uuid}/unbanByUuid")
    public void unbanUserByUuid(@PathVariable("uuid") UUID uuid) {
        userService.unbanUserByUuid(uuid);
    }

    @PostMapping
    public void createUser(@RequestBody UserCreation userCreation) {
        userService.signupUser(userCreation);
    }

    @PutMapping("/{uuid}/certificates")
    public void updateUserCertificates(@PathVariable("uuid") UUID uuid, @RequestBody List<Certificate> certificates) {
        userService.updateUserCertificates(certificates, uuid);
    }

    @PutMapping("/{uuid}/enterprise-role/{code}")
    public void setUserEnterpriseRole(@PathVariable("uuid") UUID uuid, @PathVariable("code") String roleCode) {
        userService.setUserEnterpriseRole(roleCode, uuid);
    }
}
