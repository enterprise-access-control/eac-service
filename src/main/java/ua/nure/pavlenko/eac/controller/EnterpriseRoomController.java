package ua.nure.pavlenko.eac.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ua.nure.pavlenko.eac.api.EnterpriseRoom;
import ua.nure.pavlenko.eac.service.EnterpriseRoomService;
import ua.nure.pavlenko.eac.transformer.EnterpriseRoomTransformer;

import java.util.Collection;

import static ua.nure.pavlenko.eac.util.ControllerUtils.buildResponseEntity;

@RestController
@RequestMapping("/rooms")
@PreAuthorize("hasRole('ADMIN')")
public class EnterpriseRoomController {

    private final EnterpriseRoomService enterpriseRoomService;
    private final EnterpriseRoomTransformer enterpriseRoomTransformer;

    public EnterpriseRoomController(
            EnterpriseRoomService enterpriseRoomService,
            EnterpriseRoomTransformer enterpriseRoomTransformer) {
        this.enterpriseRoomService = enterpriseRoomService;
        this.enterpriseRoomTransformer = enterpriseRoomTransformer;
    }

    @GetMapping
    public ResponseEntity<Collection<EnterpriseRoom>> getAll() {
        return buildResponseEntity(enterpriseRoomTransformer.findAll());
    }

    @GetMapping("/{code}")
    public ResponseEntity<EnterpriseRoom> getByCode(@PathVariable("code") String code) {
        return buildResponseEntity(enterpriseRoomTransformer.findByCode(code));
    }

    @PostMapping
    public void createRoom(@RequestBody EnterpriseRoom room) {
        enterpriseRoomService.createRoom(room);
    }

    @PutMapping
    public void updateRoom(@RequestBody EnterpriseRoom room) {
        enterpriseRoomService.updateRoom(room);
    }
}
