package ua.nure.pavlenko.eac.controller.handler;

import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import ua.nure.pavlenko.eac.api.ApiException;
import ua.nure.pavlenko.eac.api.AuthExceptionInformation;
import ua.nure.pavlenko.eac.security.exception.AuthException;

import javax.persistence.EntityExistsException;
import javax.persistence.EntityNotFoundException;

@Order(Ordered.HIGHEST_PRECEDENCE)
@ControllerAdvice
public class RestApiExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(EntityNotFoundException.class)
    public ResponseEntity<ApiException> handleEntityNotFound(
            EntityNotFoundException exception) {
        return buildResponseExceptionEntity(new ApiException(HttpStatus.NOT_FOUND, exception.getMessage()));
    }

    @ExceptionHandler(EntityExistsException.class)
    public ResponseEntity<ApiException> handleEntityExists(
            EntityExistsException exception) {
        return buildResponseExceptionEntity(new ApiException(HttpStatus.CONFLICT, exception.getMessage()));
    }

    @ExceptionHandler(AuthException.class)
    public ResponseEntity<ApiException> handleAuthException(
            AuthException exception) {
        return buildResponseExceptionEntity(exception.getInformation());
    }

    @ExceptionHandler(DataIntegrityViolationException.class)
    public ResponseEntity<ApiException> handleDataIntegrityViolationException(
            DataIntegrityViolationException exception) {
        return buildResponseExceptionEntity(new ApiException(HttpStatus.BAD_REQUEST, exception.getMessage()));
    }

    @ExceptionHandler(IllegalArgumentException.class)
    public ResponseEntity<ApiException> handleIllegalArgumentException(IllegalArgumentException exception) {
        return buildResponseExceptionEntity(new ApiException(HttpStatus.BAD_REQUEST, exception.getMessage()));
    }

    @ExceptionHandler(UsernameNotFoundException.class)
    public ResponseEntity<ApiException> handleUserNameNotFoundException(UsernameNotFoundException exception) {
        return buildResponseExceptionEntity(new AuthExceptionInformation());
    }

    private ResponseEntity<ApiException> buildResponseExceptionEntity(ApiException apiException) {
        return new ResponseEntity<>(apiException, apiException.getStatus());
    }
}