package ua.nure.pavlenko.eac.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ua.nure.pavlenko.eac.api.RFIDDevice;
import ua.nure.pavlenko.eac.api.RFIDDeviceDetailed;
import ua.nure.pavlenko.eac.service.RFIDDeviceService;
import ua.nure.pavlenko.eac.transformer.RFIDDeviceTransformer;

import java.util.Collection;

import static ua.nure.pavlenko.eac.util.ControllerUtils.buildResponseEntity;

@RestController
@RequestMapping("/devices")
@PreAuthorize("hasRole('ADMIN')")
public class RFIDDeviceController {

    private final RFIDDeviceTransformer deviceTransformer;
    private final RFIDDeviceService deviceService;

    public RFIDDeviceController(RFIDDeviceTransformer deviceTransformer, RFIDDeviceService deviceService) {
        this.deviceTransformer = deviceTransformer;
        this.deviceService = deviceService;
    }

    @GetMapping
    public ResponseEntity<Collection<RFIDDevice>> findAll() {
        return buildResponseEntity(deviceTransformer.findAll());
    }

    @GetMapping("/{code}/details")
    public ResponseEntity<RFIDDeviceDetailed> findDeviceDetailedByCode(@PathVariable("code") String code) {
        return buildResponseEntity(deviceTransformer.findDetailedByCode(code));
    }

    @PostMapping
    public void createRfidDevice(@RequestBody RFIDDevice rfidDevice) {
        deviceService.createRfidDevice(rfidDevice);
    }
}
