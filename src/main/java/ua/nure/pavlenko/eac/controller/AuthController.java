package ua.nure.pavlenko.eac.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ua.nure.pavlenko.eac.api.Token;
import ua.nure.pavlenko.eac.api.UserSignIn;
import ua.nure.pavlenko.eac.service.UserService;

import javax.validation.Valid;

@RestController
@RequestMapping("/auth")
public class AuthController {

    private final UserService userService;

    public AuthController(@Autowired UserService userService) {
        this.userService = userService;
    }

    @PostMapping("/sign-in")
    public ResponseEntity<Token> logIn(@Valid @RequestBody UserSignIn userSignIn) {
        return ResponseEntity.of(userService.signInUser(userSignIn));
    }
}