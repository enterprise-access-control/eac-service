package ua.nure.pavlenko.eac.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ua.nure.pavlenko.eac.api.Certificate;
import ua.nure.pavlenko.eac.service.CertificateService;

import java.util.Collection;

import static ua.nure.pavlenko.eac.util.ControllerUtils.buildResponseEntity;

@RestController
@RequestMapping("/certificates")
@PreAuthorize("hasRole('ADMIN')")
public class CertificateController {

    private final CertificateService certificateService;

    public CertificateController(CertificateService certificateService) {
        this.certificateService = certificateService;
    }

    @GetMapping
    public ResponseEntity<Collection<Certificate>> getAll() {
        return buildResponseEntity(certificateService.findAllModels());
    }

    @GetMapping("/{code}")
    public ResponseEntity<Certificate> findByCode(@PathVariable("code") String code) {
        return buildResponseEntity(certificateService.findByCode(code));
    }

    @PostMapping
    public void createCertificate(@RequestBody Certificate certificate) {
        certificateService.createCertificate(certificate);
    }
}
