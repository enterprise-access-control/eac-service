package ua.nure.pavlenko.eac.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ua.nure.pavlenko.eac.api.EnterpriseRole;
import ua.nure.pavlenko.eac.service.EnterpriseRoleService;

import java.util.Collection;

import static ua.nure.pavlenko.eac.util.ControllerUtils.buildResponseEntity;
import static ua.nure.pavlenko.eac.util.EnterpriseRoleTransformerUtils.transformToEnterpriseRole;

@RestController
@RequestMapping("/enterprise-roles")
@PreAuthorize("hasRole('ADMIN')")
public class EnterpriseRoleController {

    private final EnterpriseRoleService enterpriseRoleService;

    public EnterpriseRoleController(EnterpriseRoleService enterpriseRoleService) {
        this.enterpriseRoleService = enterpriseRoleService;
    }

    @GetMapping
    public ResponseEntity<Collection<EnterpriseRole>> findAll() {
        return buildResponseEntity(enterpriseRoleService.findAll());
    }

    @GetMapping("/{code}")
    public ResponseEntity<EnterpriseRole> findByCode(@PathVariable("code") String code) {
        EnterpriseRole enterpriseRole =
                transformToEnterpriseRole(enterpriseRoleService.findByCode(code, true));
        return buildResponseEntity(enterpriseRole);
    }

    @PostMapping
    public void createEnterpriseRole(@RequestBody EnterpriseRole enterpriseRole) {
        enterpriseRoleService.createEnterpriseRole(enterpriseRole);
    }

    @DeleteMapping("/{code}")
    public void deleteByCode(@PathVariable("code") String code) {
        enterpriseRoleService.deleteEnterpriseRoleByCode(code);
    }
}
