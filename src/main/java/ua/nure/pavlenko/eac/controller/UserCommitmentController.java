package ua.nure.pavlenko.eac.controller;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ua.nure.pavlenko.eac.api.UserCommitment;
import ua.nure.pavlenko.eac.service.UserCommitmentService;

@RestController
@RequestMapping("/commitments")
@PreAuthorize("hasRole('ADMIN')")
public class UserCommitmentController {

    private final UserCommitmentService commitmentService;

    public UserCommitmentController(UserCommitmentService commitmentService) {
        this.commitmentService = commitmentService;
    }

    @PostMapping
    public void createUserCommitment(@RequestBody UserCommitment userCommitment) {
        commitmentService.createUserCommitment(userCommitment);
    }
}
