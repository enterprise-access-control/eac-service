package ua.nure.pavlenko.eac.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ua.nure.pavlenko.eac.api.EventProcessingResult;
import ua.nure.pavlenko.eac.api.RFIDOperationEvent;
import ua.nure.pavlenko.eac.processor.EventProcessor;

import static ua.nure.pavlenko.eac.util.ControllerUtils.buildEventProcessingResponse;

@RestController
@RequestMapping("/rfid-device-operations")
public class RFIDDeviceOperationController {

    private final EventProcessor<RFIDOperationEvent> processor;

    public RFIDDeviceOperationController(EventProcessor<RFIDOperationEvent> processor) {
        this.processor = processor;
    }

    @PostMapping
    public ResponseEntity<EventProcessingResult> processRfidDeviceOperationEvent(
            @RequestBody RFIDOperationEvent event) {
        return buildEventProcessingResponse(processor.processEvent(event));
    }
}
