package ua.nure.pavlenko.eac.validator.access;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.stereotype.Component;
import ua.nure.pavlenko.eac.repository.entity.CertificateEntity;
import ua.nure.pavlenko.eac.repository.entity.UserEntity;

import java.util.Collection;
import java.util.Set;

import static org.apache.commons.collections4.CollectionUtils.containsAll;
import static org.apache.commons.collections4.CollectionUtils.emptyCollection;

@Component
public class CertificateAccessValidator implements AccessValidator<CertificateEntity> {

    @Override
    public boolean validateAccess(Collection<CertificateEntity> expectedValues, UserEntity userEntity) {
        if (CollectionUtils.isEmpty(expectedValues)) {
            return true;
        }

        Set<CertificateEntity> userCertificates = userEntity.getCertificates();
        userCertificates.addAll(userEntity.getEnterpriseRole() != null
                                        ? userEntity.getEnterpriseRole().getCertificates()
                                        : emptyCollection());

        return containsAll(userEntity.getCertificates(), expectedValues);
    }
}
