package ua.nure.pavlenko.eac.validator.access;

import ua.nure.pavlenko.eac.repository.entity.UserEntity;

import java.util.Collection;

public interface AccessValidator<T> {

    boolean validateAccess(Collection<T> expectedValues, UserEntity userEntity);
}
