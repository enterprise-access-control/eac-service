package ua.nure.pavlenko.eac.validator.operation;

public interface OperationValidator<T> {

    boolean validateOperationAccess(T object);
}
