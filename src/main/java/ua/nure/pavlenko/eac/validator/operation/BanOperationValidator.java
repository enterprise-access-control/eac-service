package ua.nure.pavlenko.eac.validator.operation;

import org.springframework.stereotype.Component;
import ua.nure.pavlenko.eac.repository.entity.SystemRole;
import ua.nure.pavlenko.eac.repository.entity.UserEntity;

import java.util.List;

@Component
public class BanOperationValidator implements OperationValidator<UserEntity> {

    private final List<SystemRole> expectedRoles = List.of(SystemRole.USER);

    @Override
    public boolean validateOperationAccess(UserEntity user) {
        return expectedRoles.contains(user.getSystemRole().getRole());
    }
}
