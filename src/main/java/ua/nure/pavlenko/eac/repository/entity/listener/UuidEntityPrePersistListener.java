package ua.nure.pavlenko.eac.repository.entity.listener;

import com.sun.istack.NotNull;
import ua.nure.pavlenko.eac.repository.entity.UuidEntity;

import javax.persistence.PrePersist;
import java.util.UUID;

import static java.util.Objects.isNull;

public class UuidEntityPrePersistListener {

    @PrePersist
    public void generateUuidIfNotExist(@NotNull UuidEntity uuidEntity) {
        if (isNull(uuidEntity.getUuid())) {
            uuidEntity.setUuid(UUID.randomUUID());
        }
    }
}
