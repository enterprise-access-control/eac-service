package ua.nure.pavlenko.eac.repository.entity;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import java.util.Set;

@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
@Entity(name = "enterprise_room")
@Accessors(chain = true)
public class EnterpriseRoomEntity extends BaseEntity {

    @Column(nullable = false, unique = true)
    private String code;

    @Column(name = "is_tracking_time_zone")
    private boolean trackingTimeZone;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "enterprise_room_certificate",
            joinColumns = {
                    @JoinColumn(name = "enterprise_room_id", referencedColumnName = "id",
                            nullable = false, updatable = false)},
            inverseJoinColumns = {
                    @JoinColumn(name = "certificate_id", referencedColumnName = "id",
                            nullable = false, updatable = false)})
    private Set<CertificateEntity> certificates;
}
