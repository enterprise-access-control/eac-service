package ua.nure.pavlenko.eac.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ua.nure.pavlenko.eac.repository.entity.RFIDDeviceEntity;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface RFIDDeviceRepository extends JpaRepository<RFIDDeviceEntity, Long> {

    Optional<RFIDDeviceEntity> findByUuid(UUID uuid);

    Optional<RFIDDeviceEntity> findByCode(String code);

    boolean existsByCode(String code);
}
