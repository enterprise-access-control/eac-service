package ua.nure.pavlenko.eac.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ua.nure.pavlenko.eac.repository.entity.EnterpriseRoleEntity;

import java.util.Optional;

@Repository
public interface EnterpriseRoleRepository extends JpaRepository<EnterpriseRoleEntity, Long> {

    Optional<EnterpriseRoleEntity> findByCode(String code);

    boolean existsByCode(String code);
}
