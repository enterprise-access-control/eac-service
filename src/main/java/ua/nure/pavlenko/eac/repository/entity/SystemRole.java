package ua.nure.pavlenko.eac.repository.entity;

public enum SystemRole {

    USER, ADMIN;

    public String getRoleName() {
        return "ROLE_" + this.toString();
    }
}
