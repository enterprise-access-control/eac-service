package ua.nure.pavlenko.eac.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ua.nure.pavlenko.eac.repository.entity.UserCommitmentEntity;

@Repository
public interface UserCommitmentRepository extends JpaRepository<UserCommitmentEntity, Long> {
}
