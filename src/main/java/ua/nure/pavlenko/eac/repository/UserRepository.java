package ua.nure.pavlenko.eac.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ua.nure.pavlenko.eac.repository.entity.SystemRole;
import ua.nure.pavlenko.eac.repository.entity.UserEntity;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface UserRepository extends JpaRepository<UserEntity, Long> {

    Optional<UserEntity> findByUuid(UUID uuid);

    Optional<UserEntity> findByEmail(String email);

    Optional<UserEntity> findByLogin(String login);

    List<UserEntity> findAllByBanned(boolean isBanned);

    List<UserEntity> findAllByBannedAndSystemRole_Role(boolean isBanned, SystemRole role);
}
