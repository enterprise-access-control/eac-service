package ua.nure.pavlenko.eac.repository.entity;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
@Entity(name = "rfid_device")
@Accessors(chain = true)
public class RFIDDeviceEntity extends UuidEntity {

    @Column
    private String code;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "enter_room_id")
    private EnterpriseRoomEntity enterRoom;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "exit_room_id")
    private EnterpriseRoomEntity exitRoom;
}
