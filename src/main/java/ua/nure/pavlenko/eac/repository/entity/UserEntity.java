package ua.nure.pavlenko.eac.repository.entity;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import java.util.List;
import java.util.Set;

@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
@Entity(name = "users")
@Accessors(chain = true)
public class UserEntity extends UuidEntity {

    @Column(nullable = false)
    private String email;

    @Column(nullable = false)
    private String login;

    @Column(nullable = false)
    private String password;

    @Column
    private String name;

    @Column
    private String surname;

    @Column(name = "is_banned")
    private boolean banned;

    @Column(name = "worked_this_month")
    private Long workedThisMonth;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "user")
    private List<UserCommitmentEntity> userCommitments;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "system_role_id")
    private SystemRoleEntity systemRole;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "enterprise_role_id")
    private EnterpriseRoleEntity enterpriseRole;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "user_certificate",
            joinColumns = {
                    @JoinColumn(name = "user_id", referencedColumnName = "id",
                            nullable = false, updatable = false)},
            inverseJoinColumns = {
                    @JoinColumn(name = "certificate_id", referencedColumnName = "id",
                            nullable = false, updatable = false)})
    private Set<CertificateEntity> certificates;

    public Long getUserEstimatedWorkHoursInMillis() {
        return userCommitments.stream()
                              .map(UserCommitmentEntity::getRequiredTimeInMonth)
                              .mapToLong(Long::longValue)
                              .sum();
    }
}
