package ua.nure.pavlenko.eac.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ua.nure.pavlenko.eac.repository.entity.SystemRole;
import ua.nure.pavlenko.eac.repository.entity.SystemRoleEntity;

import java.util.Optional;

@Repository
public interface SystemRoleRepository extends JpaRepository<SystemRoleEntity, Long> {

    Optional<SystemRoleEntity> findByRole(SystemRole role);
}
