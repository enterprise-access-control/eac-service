package ua.nure.pavlenko.eac.repository.entity;

public enum RFIDDeviceOperationType {

    ENTER_APPROVED, ENTER_DECLINED, EXIT_APPROVED
}
