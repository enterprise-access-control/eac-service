package ua.nure.pavlenko.eac.repository.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
public class Permission {

    private Module module;
    private String operation;
}
