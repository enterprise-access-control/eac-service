package ua.nure.pavlenko.eac.repository.entity;

public enum Operation {

    READ, CREATE, UPDATE, DELETE;
}
