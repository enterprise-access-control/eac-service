package ua.nure.pavlenko.eac.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ua.nure.pavlenko.eac.repository.entity.EnterpriseRoomEntity;

import java.util.Optional;

public interface EnterpriseRoomRepository extends JpaRepository<EnterpriseRoomEntity, Long> {

    Optional<EnterpriseRoomEntity> findByCodeIgnoreCase(String code);

    boolean existsByCode(String code);
}
