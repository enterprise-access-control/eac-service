package ua.nure.pavlenko.eac.repository.entity;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.persistence.Column;
import javax.persistence.Entity;

@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
@Entity(name = "certificate")
@Accessors(chain = true)
public class CertificateEntity extends BaseEntity {

    @Column(nullable = false, unique = true)
    private String code;

    @Column
    private String description;
}
