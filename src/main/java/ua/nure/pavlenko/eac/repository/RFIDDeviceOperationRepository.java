package ua.nure.pavlenko.eac.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ua.nure.pavlenko.eac.repository.entity.RFIDDeviceEntity;
import ua.nure.pavlenko.eac.repository.entity.RFIDDeviceOperationEntity;
import ua.nure.pavlenko.eac.repository.entity.UserEntity;

import java.sql.Timestamp;
import java.util.List;
import java.util.Optional;

public interface RFIDDeviceOperationRepository extends JpaRepository<RFIDDeviceOperationEntity, Long> {

    Optional<RFIDDeviceOperationEntity> findFirstByUserEntityOrderByOperationTimestampDesc(UserEntity userEntity);

    List<RFIDDeviceOperationEntity> findByRfidDeviceEntityAndOperationTimestampGreaterThan(
            RFIDDeviceEntity rfidDeviceEntity, Timestamp timestamp);

    List<RFIDDeviceOperationEntity> findAllByOperationTimestampGreaterThanAndUserEntity(
            Timestamp timestamp, UserEntity userEntity);
}
