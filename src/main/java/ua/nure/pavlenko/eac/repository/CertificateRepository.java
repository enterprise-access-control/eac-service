package ua.nure.pavlenko.eac.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ua.nure.pavlenko.eac.repository.entity.CertificateEntity;

import java.util.List;
import java.util.Optional;
import java.util.Set;

public interface CertificateRepository extends JpaRepository<CertificateEntity, Long> {

    Set<CertificateEntity> findAllByCodeInIgnoreCase(List<String> codes);

    Optional<CertificateEntity> findByCodeIgnoreCase(String code);

    boolean existsByCode(String code);
}
