package ua.nure.pavlenko.eac.repository.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Type;
import ua.nure.pavlenko.eac.repository.entity.listener.UuidEntityPrePersistListener;

import javax.persistence.Column;
import javax.persistence.EntityListeners;
import javax.persistence.MappedSuperclass;
import java.util.UUID;


@Data
@EqualsAndHashCode(callSuper = true, of = "uuid")
@MappedSuperclass
@EntityListeners(UuidEntityPrePersistListener.class)
public abstract class UuidEntity extends BaseEntity {

    @Column(nullable = false)
    @Type(type = "org.hibernate.type.PostgresUUIDType")
    private UUID uuid;
}
