package ua.nure.pavlenko.eac.repository.entity;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.jetbrains.annotations.NotNull;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import java.sql.Timestamp;

@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
@Entity(name = "rfid_device_operation")
@Accessors(chain = true)
public class RFIDDeviceOperationEntity extends BaseEntity implements Comparable<RFIDDeviceOperationEntity> {

    @ManyToOne(fetch = FetchType.EAGER)
    private RFIDDeviceEntity rfidDeviceEntity;

    @ManyToOne(fetch = FetchType.EAGER)
    private UserEntity userEntity;

    @Column(name = "rfid_device_operation_type")
    @Enumerated(EnumType.STRING)
    private RFIDDeviceOperationType operationType;

    @Column(name = "operation_timestamp")
    private Timestamp operationTimestamp;

    @Override
    public int compareTo(@NotNull RFIDDeviceOperationEntity o) {
        return Long.compare(operationTimestamp.getTime(), o.operationTimestamp.getTime());
    }

    public EnterpriseRoomEntity getEnterRoom() {
        return rfidDeviceEntity.getEnterRoom();
    }
}
