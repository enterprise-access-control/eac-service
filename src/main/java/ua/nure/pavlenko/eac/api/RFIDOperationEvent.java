package ua.nure.pavlenko.eac.api;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import ua.nure.pavlenko.eac.repository.entity.RFIDDeviceOperationType;

import javax.validation.constraints.NotNull;
import java.util.UUID;

@Getter
@Setter
@Accessors(chain = true)
public class RFIDOperationEvent {

    @NotNull
    private UUID rfidDeviceUuid;
    @NotNull
    private UUID userUuid;
    @NotNull
    private Long timestamp;
    @NotNull
    private RFIDDeviceOperationType operationType;
}
