package ua.nure.pavlenko.eac.api;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.util.List;
import java.util.UUID;

@Getter
@Setter
@Accessors(chain = true)
public class UserDetailed extends User {

    private UUID uuid;
    private List<Certificate> certificates;
    private EnterpriseRole enterpriseRole;
    private UserRfidOperation userRfidOperation;
    private Long workedHoursThisMonth;
    private Long estimatedHoursThisMonth;
}
