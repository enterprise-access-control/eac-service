package ua.nure.pavlenko.eac.api;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import ua.nure.pavlenko.eac.repository.entity.SystemRole;

import java.util.Calendar;

@Getter
@Setter
@Accessors(chain = true)
public class User {

    private String firstName;
    private String secondName;
    private String email;
    private String login;
    private SystemRole role;
    private Calendar registrationTimestamp;
    private Calendar lastUpdateTimestamp;
    private Boolean banned;
}
