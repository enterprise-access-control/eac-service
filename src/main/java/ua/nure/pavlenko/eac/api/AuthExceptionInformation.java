package ua.nure.pavlenko.eac.api;

import org.springframework.http.HttpStatus;

public class AuthExceptionInformation extends ApiException {

    public AuthExceptionInformation() {
        super(HttpStatus.FORBIDDEN, "Wrong email or password");
    }
}