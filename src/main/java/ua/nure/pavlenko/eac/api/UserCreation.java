package ua.nure.pavlenko.eac.api;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;
import ua.nure.pavlenko.eac.repository.entity.SystemRole;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@Getter
@Setter
public class UserCreation {

    @Email
    private String email;
    @Length(min = 3, max = 16)
    private String login;
    private String firstName;
    private String lastName;
    @Pattern(regexp = "^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$")
    private String password;
    @NotNull
    private SystemRole role;
}
