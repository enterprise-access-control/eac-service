package ua.nure.pavlenko.eac.api;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;

@Getter
@Setter
@Accessors(chain = true)
public class UserCommitment {

    @NotEmpty
    private String roomCode;
    @NotEmpty
    private String login;
    @Min(1000)
    private Long requiredTimeInMillisOnMonth;
}
