package ua.nure.pavlenko.eac.api;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import ua.nure.pavlenko.eac.repository.entity.RFIDDeviceOperationType;

import java.sql.Timestamp;
import java.util.Calendar;

@Getter
@Setter
@Accessors(chain = true)
public class UserRfidOperation {

    private String login;
    private String deviceCode;
    private String currentRoomCode;
    private Timestamp operationTimestamp;
    private RFIDDeviceOperationType operationType;
    private Calendar creationTimestamp;
}
