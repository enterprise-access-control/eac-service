package ua.nure.pavlenko.eac.api;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public class Token {

    private final String token;
}
