package ua.nure.pavlenko.eac.api;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.util.Calendar;
import java.util.List;
import java.util.UUID;

@Getter
@Setter
@Accessors(chain = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class RFIDDeviceDetailed extends RFIDDevice {

    private UUID uuid;
    private List<Certificate> expectedEnterCertificates;
    private List<Certificate> expectedExitCertificates;
    private List<UserRfidOperation> lastUserOperations;
    private Calendar creationTimestamp;
    private Calendar updatedTimestamp;
}
