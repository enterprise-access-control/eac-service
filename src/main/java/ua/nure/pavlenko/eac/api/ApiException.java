package ua.nure.pavlenko.eac.api;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.springframework.http.HttpStatus;

@Getter
@Setter
@RequiredArgsConstructor
public class ApiException {

    private final HttpStatus status;
    private final String message;
}
