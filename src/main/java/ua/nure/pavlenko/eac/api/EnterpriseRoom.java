package ua.nure.pavlenko.eac.api;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotEmpty;
import java.util.List;

@Getter
@Setter
@Accessors(chain = true)
public class EnterpriseRoom {

    @NotEmpty
    private String code;
    private Boolean trackingTimeZone;
    private List<Certificate> certificates;
}
