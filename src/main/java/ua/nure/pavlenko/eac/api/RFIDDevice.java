package ua.nure.pavlenko.eac.api;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotEmpty;

@Getter
@Setter
@Accessors(chain = true)
public class RFIDDevice {

    @NotEmpty
    private String code;
    @NotEmpty
    private String exitRoomCode;
    @NotEmpty
    private String enterRoomCode;
}
