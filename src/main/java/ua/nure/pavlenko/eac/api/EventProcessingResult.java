package ua.nure.pavlenko.eac.api;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Accessors(chain = true)
public class EventProcessingResult {

    private boolean success = false;
    private List<String> exceptions = new ArrayList<>();

    public EventProcessingResult addExceptionMessage(String message) {
        exceptions.add(message);
        return this;
    }
}
